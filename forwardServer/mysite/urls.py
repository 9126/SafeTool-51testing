#!/usr/bin/env python
# -*- coding:utf-8 -*-
#url路径匹配
get_url = {
    #例子
    #"/appname/path":"apis&apiName"
    "/nmap/help":"apis&nmap_help",
    #app:xxe
    "/xxe/attack.dtd":"apis&xxefile",
    #app:xss
    "/xss/xsstester":"apis&xsstester",
    #app:cvedb
    "/cvedb":"apis&nvddata",
    #app:csrf
    "/csrf/getcsrfpage":"apis&get_csrf_page",
    "/csrf/postcsrfpage":"apis&post_csrf_page",
    "/csrf/postjsoncsrfpage":"apis&postjson_csrf_page",
    "/csrf/formjsoncsrfpage":"apis&formjson_csrf_page"
}
post_url = {
    #app:csrf
    "/csrf/getcsrf":"apis&exec_get_csrf",
    "/csrf/postcsrf":"apis&exec_post_csrf",
    "/csrf/postjsoncsrf":"apis&exec_postjson_csrf",
    "/csrf/formjsoncsrf":"apis&exec_formjson_csrf",
    #app:nmap
    "/nmap/ping":"apis&nmap_ping",
    "/nmap/ping/range":"apis&nmap_ping_range",
    "/nmap/ping/not/port":"apis&nmap_ping_not_port",
    "/nmap/ack":"apis&nmap_ack",
    "/nmap/traceroute":"apis&nmap_traceroute",
    "/nmap/syn":"apis&nmap_syn",
    "/nmap/tcp":"apis&nmap_tcp",
    "/nmap/udp":"apis&nmap_udp",
    "/nmap/fin":"apis&nmap_fin",
    "/nmap/xmasTree":"apis&nmap_xmasTree",
    "/nmap/null":"apis&nmap_null",
    "/nmap/set/port/range":"apis&nmap_set_port_range",
    "/nmap/specify/port/range":"apis&nmap_specify_port_range",
    "/nmap/os/version":"apis&nmap_os_version",
    "/nmap/all/scan":"apis&nmap_all_scan",
    "/nmap/custom":"apis&nmap_custom",
    #app:dirsearch
    "/dirsearch/scan":"apis&scan_dir",
    "/dirsearch/report":"apis&send_report"

}