#!/usr/bin/env python
# -*- coding:utf-8 -*-
import subprocess
import chardet
def cmdExec(cmd):
    r = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    for line in iter(r.stdout.readline,b''):
        if type(line) == bytes:
            if chardet.detect(line)["encoding"] == "GB2312":
                line = line.strip().decode('gbk')
            else:
                line = bytes.decode(line)
        yield line

def cmdExecCom(cmd):
    r = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    return r