#!/usr/bin/env python
# -*- coding:utf-8 -*-

from libs import printer
from conf.setting import DICT_PATH
import os
import traceback
import random
import datetime

def show_menu():
    printer.info("字典选项: ")
    printer.plus("1. 数字类型")
    printer.plus("2. 日期类型")

def show_date_menu():
    printer.info("选项: ")
    printer.plus("1.自定义")
    printer.plus("2.指定范围")
  
def show_date_custom_menu():
    printer.info("选项: ")
    printer.plus("1.年份(y)")
    printer.plus("2.月份(m)")
    printer.plus("3.天数(d)")

def generate_digital_model(start,end,step=1):
    digs = []
    if step == 1:
        digs = [ str(i) for i in range(int(start),int(end)+1)]
        digs = [(len(str(start))-len(d))*'0'+ d for d in digs]
    else:
        for i in range(int(start),int(end)+1,2):
            if i >= int(end):
                digs.append(str(end))
                break
            digs.append(str(i))
    digs = [(len(str(start))-len(d))*'0'+ d for d in digs]
    return digs
def generate_date_year(start,end,step=1):
    year = generate_digital_model(start,end,step)
    return year
def generate_date_month(start,end,step=1):
    month = generate_digital_model(start,end,step)
    return month
def generate_date_days(start,end,step=1):
    days = generate_digital_model(start,end,step)
    return days
def generate_date_model(year,month,day,step='y',sep="-"):
    result = []
    if step == "y":
        if year:
            for y in year:
                if day:
                    sf = y + sep + month + sep + day
                else:
                    sf = y + sep + month
                result.append(sf)
    elif step == "m":
        if month:
            for m in month:
                if day:
                    sf = year + sep + m + sep + day
                else:
                    sf = year + sep + m
                result.append(sf)
    elif step == "d":
        if day:
            for d in day:
                sf = year + sep + month + sep + d
                result.append(sf)
    return result
def generate_range_date_model(start,end,sep="-"):
    result = []
    if sep == "-":
        md = "-01-01"
        fm = "%Y-%m-%d"
    elif sep == "/":
        md = "/01/01"
        fm = "%Y/%m/%d"
    start = datetime.datetime.strptime(start + md,fm)
    end = datetime.datetime.strptime(end + md, fm)
    for r in range(0,(end-start).days):
        day = start + datetime.timedelta(days=r)
        result.append(day.strftime(fm))
    return result
def output_file(filename,result):
    filepath = DICT_PATH + os.sep + filename
    with open(filepath,"w",encoding="utf-8") as fp:
        for r in result:
            fp.write(r + "\n")
    printer.plus("字典写入成功: " + filepath)
def execute(args):
    try:
        result = []
        printer.warn("自定义生成字典,后续添加更多处理逻辑!")
        show_menu()
        op = input("请输入选项: ")
        op = op.strip()
        if op == "1":
            start = input("输入起始值: ").strip()
            end = input("输入结束值: ").strip()
            step = input("输入步长: ").strip()
            step = int(step)
            result = generate_digital_model(start,end,step)
        elif op == "2":
            show_date_menu()
            opd = input("请输入选项: ").strip()
            if opd == "1":
                basey = input("请输入基线年份: ")
                basem = input("请输入基线月份: ")
                based = input("请输入基线天数: ")
                bases = input("请输入连接符(-或/): ")
                show_date_custom_menu()
                opdc = input("请输入选项: ").strip()
                if opdc == "1":
                    endy = input("请输入结束年份: ").strip()
                    ends = input("请输入步长: ").strip()
                    ends = int(ends)
                    year = generate_date_year(basey,endy,ends)
                    result = generate_date_model(year,basem,based,"y",bases)
                elif opdc == "2":
                    endm = input("请输入结束月份: ").strip()
                    ends = input("请输入步长: ").strip()
                    ends = int(ends)
                    month = generate_date_month(basem,endm,ends)
                    result = generate_date_model(basey,month,based,"m",bases)
                elif opdc == "3":
                    endd = input("请输入结束天数: ").strip()
                    ends = input("请输入步长: ").strip()
                    ends = int(ends)
                    days = generate_date_days(based,endd,ends)
                    result = generate_date_model(basey,basem,days,"d",bases)
            elif opd == "2":
                start = input("输入开始年份: ").strip()
                end = input("输入结束年份: ").strip()
                sep = input("请输入连接符(-或/): ")
                result = generate_range_date_model(start,end,sep)
        if result:
            for i in result:
                printer.info(i)
            filename = input("请输入字典名: ").strip()
            filename = filename + ".txt"
            output_file(filename,result)
    except Exception as e:
        traceback.print_exc()
