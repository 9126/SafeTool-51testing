#!/usr/bin/env python
# -*- coding:utf-8 -*-
import time

def get_now_timeStamp():
    try:
        timeStamp = int(time.mktime(time.localtime()))
        return timeStamp
    except Exception as e:
        return False

def get_timeStamp(tStr:str):
    try:
        timeArray = time.strptime(tStr, "%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        return timeStamp
    except Exception as e:
        return False

def timeStamp_to_time(timeStamp:str):
    try:
        timeArray = time.localtime(int(timeStamp))
        tStr = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        return tStr
    except Exception as e:
        return False

def execute(args):
    try:
        print("1. 获得当前时间戳")
        print("2. 按指定时间转换时间戳(ex: 2019-12-12 12:12:00)")
        print("3. 时间戳转换成常规时间")
        ops = input("请输入选项: ")
        ops = ops.strip()
        if ops == "1":
            result = get_now_timeStamp()
            if result:
                print("结果: " + str(result))
            else:
                print("转换错误!")
        elif ops == "2":
            t = input("请输入时间: ")
            result = get_timeStamp(t.strip())
            if result:
                print("结果: " + str(result))
            else:
                print("输入错误!")
        elif ops == "3":
            t = input("请输入时间戳: ")
            t = t.strip()
            result = timeStamp_to_time(t)
            if result:
                print("结果: " + str(result))
            else:
                print("输入错误!")
        else:
            print("请输入选项的序号!")
    except Exception as e:
        import traceback
        traceback.print_exc()
