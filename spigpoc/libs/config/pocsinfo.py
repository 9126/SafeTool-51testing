#!/usr/bin/env python
# -*- coding:utf-8 -*-
import yaml
import os
import re
from libs.config.settings import DB_CONF_POC,PATHS_POCS
class pocsinfo:
    def __init__(self):
        self.yamlPath = DB_CONF_POC
        rFile = open(self.yamlPath,'r',encoding='utf-8')
        content = rFile.read()
        self.conf = yaml.load(content,Loader=yaml.FullLoader)
    def get_pocs(self):
        keys = []
        for k in self.conf.keys():
            keys.append(k)
        return keys
    def get_pocs_items(self,name:str):
        keys = []
        if name in self.conf.keys():
            for k in self.conf[name].keys():
                keys.append(k)
        return keys
    def get_pocs_path(self,name:str):
        path = ""
        if name in self.conf.keys():
            path = PATHS_POCS + os.sep + name
        return path
    def get_poc_path(self,name:str):
        path = ""
        if name.find(".") > 0:
            model,item = name.split(".")
            if model in self.conf.keys():
                if item in self.conf[model].keys():
                    path = PATHS_POCS + os.sep + model + os.sep + item + ".py"
        return path
    def get_poc_info(self,name:str):
        path = self.get_poc_path(name)
        poc_info = {}
        if path:
            model,item = name.split(".")
            poc_info = self.conf[model][item]
        return poc_info
