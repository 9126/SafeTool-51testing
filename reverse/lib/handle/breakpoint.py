#!/usr/bin/env python
# -*- coding:utf-8 -*-
import ctypes

DBG_CONTINUE = 0x00010002

def handler_breakpoint64(debugObj:object):
    print("调试地址: 0x%08x" % (debugObj.exception_address))
    continue_status = DBG_CONTINUE
    if debugObj.exception_address not in debugObj.breakpoints.keys():
        if debugObj.first_breakpoints == True:
            debugObj.first_breakpoints = False
            print("[*] 系统断点")
            return DBG_CONTINUE
    else:
        print("[*]自定义断点")
        debugObj.write_process_memory(debugObj.exception_address,debugObj.breakpoints[debugObj.exception_address][1])
        if debugObj.context:
            debugObj.general_register_info64(debugObj.context)
            debugObj.flags_register_info64(debugObj.context)
            debugObj.segment_register_info64(debugObj.context)
            debugObj.eflags_register(debugObj.context)
            debugObj.context.Rip -= 1
            debugObj.context.EFlags |= 0x100
        ctypes.windll.kernel32.SetThreadContext(debugObj.h_thread,ctypes.byref(debugObj.context))
    return continue_status

def run(debugObj:object,flag):
    continue_status = DBG_CONTINUE
    if flag == 64:
        continue_status = handler_breakpoint64(debugObj)
    else:
        print("参数错误!")
    return continue_status