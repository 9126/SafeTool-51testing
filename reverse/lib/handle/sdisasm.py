#!/usr/bin/env python
# -*- coding:utf-8 -*-
import ctypes
import time
from capstone import *

DBG_CONTINUE = 0x00010002
def disasm_code(data,addr):
    disasm = Cs(CS_ARCH_X86, CS_MODE_64)
    for i in disasm.disasm(data,addr):
        print("0x%x:\t%s\t%s" %(i.address, i.mnemonic, i.op_str))

def debug_single_step64(debugObj:object):
    print("[*] 单步调试地址: 0x%08x" % debugObj.exception_address)
    if debugObj.context:
        debugObj.general_register_info64(debugObj.context)
        debugObj.eflags_register(debugObj.context)
        debugObj.flags_register_info64(debugObj.context)
        debugObj.segment_register_info64(debugObj.context)
        debugObj.context.EFlags |= 0x100
        codeData = debugObj.read_process_memory(debugObj.exception_address,8)
        if codeData:
            disasm_code(codeData,debugObj.exception_address)
        else:
            print("非用户代码执行区!")
    ctypes.windll.kernel32.SetThreadContext(debugObj.h_thread,ctypes.byref(debugObj.context))
    time.sleep(1)
    return DBG_CONTINUE

def run(debugObj:object,flag):
    continue_status = DBG_CONTINUE
    if flag == 64:
        continue_status =debug_single_step64(debugObj)
    else:
        print("参数错误!")
    return continue_status