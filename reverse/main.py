#!/usr/bin/env python
# -*- coding:utf-8 -*-

from cmd import Cmd
import os
import sys
try:
    import pefile,capstone
except ImportError:
    print("请先安装第三方包:pefile和capstone")
    end = input("回车结束:")
    sys.exit()
import localapi
from lib.utils import printer
import traceback
class Cli(Cmd):
    def __init__(self):
        os.system("cls")
        banner = '''
         ____
        |  _ \ _____   _____ _ __ ___  ___ 
        | |_) / _ \ \ / / _ \ '__/ __|/ _ \\
        |  _ <  __/\ V /  __/ |  \__ \  __/
        |_| \_\___| \_/ \___|_|  |___/\___|
        '''
        Cmd.__init__(self)
        self.intro = banner
        self.prompt = 'ReverseConsole>> '
        self.fpath = ""
        self.hmodule = {
                        'bp':"breakpoint.py",
                        'st':'singlestep.py'
                       }
        self.software_bp_addr = None
    def do_menu(self,param):
        pass
    def do_set(self,param):
        param = param.strip()
        if param == "path":
            fpath = input("请输入文件地址: ")
            isExist = localapi.is_file_exists(fpath.strip())
            if isExist:
                self.fpath = fpath
                printer.info("文件路径设置成功!")
            else:
                printer.warn("文件不存在!")
        elif param == "handle":
            self.sethandle()
        else:
            printer.warn("命令存在!")

    def sethandle(self):
        localapi.api_debug_show_hmodule()
        bhandles = localapi.api_debug_get_bpmoudle()
        shandles = localapi.api_debug_get_ssmoudle()
        inp = input("断点处理模块: ")
        inp = inp.strip()
        if inp not in bhandles:
            printer.warn("模块不存在,使用默认处理模块.")
        else:
            self.hmodule['bp'] = inp
        inp = input("单步调试处理模块: ")
        inp = inp.strip()
        if inp not in shandles:
            printer.warn("模块不存在,使用默认处理模块.")
        else:
            self.hmodule['st'] = inp
    def do_setsbpaddr(self,param):
        param = param.strip()
        self.software_bp_addr = param

    def do_peinfo(self,param):
        param = param.strip()
        if self.fpath:
            if param == "headers":
                localapi.api_pe_headers(self.fpath)
            elif param == "sections":
                localapi.api_pe_sections(self.fpath)
            elif param == "impdll":
                localapi.api_pe_imp_dll_info(self.fpath)
            elif param.find(".dll") > 0:
                localapi.api_pe_dll_info(self.fpath,param)
        else:
            printer.warn("请先设置文件路径!")
    def do_peanalysis(self,param):
        param = param.strip()
        if self.fpath:
            if param == "strings":
                localapi.api_pe_extract_string(self.fpath)
            elif param.find("str") == 0 and param.find("@") > 0:
                i,s,e = param.split("@")
                localapi.api_pe_extract_string_range(self.fpath,int(s),int(e))
            elif param == "strmal":
                localapi.api_pe_find_str_mal(self.fpath)
            elif param == "funcmal":
                localapi.api_pe_find_func_mal(self.fpath)
        else:
            printer.warn("请先设置文件路径!")
        print("")
    def do_debugger(self,param):
        param = param.strip()
        if self.fpath:
            if param == "start":
                localapi.api_debug_run(self.fpath,self.hmodule,self.software_bp_addr)
        else:
            printer.warn("请先设置文件路径!")
    def do_calc(self,param):
        param = param.strip()
        result = ""
        if param == "b2d":
            b = input("输入二进制数: ")
            b = b.strip()
            result = localapi.api_calc_b2d(b)
        elif param == "b2o":
            b = input("输入二进制数: ")
            b = b.strip()
            result = localapi.api_calc_b2o(b)
        elif param == "b2h":
            b = input("输入二进制数: ")
            b = b.strip()
            result = localapi.api_calc_b2h(b)
        elif param == "d2b":
            b = input("输入十进制数: ")
            b = b.strip()
            result = localapi.api_calc_d2b(b)
        elif param == "d2o":
            b = input("输入十进制数: ")
            b = b.strip()
            result = localapi.api_calc_d2o(b)
        elif param == "d2h":
            b = input("输入十进制数: ")
            b = b.strip()
            result = localapi.api_calc_d2h(b)
        elif param == "h2b":
            b = input("输入十六进制数[0x开头]: ")
            b = b.strip()
            result = localapi.api_calc_h2b(b)
        elif param == "h2o":
            b = input("输入十六进制数[0x开头]: ")
            b = b.strip()
            result = localapi.api_calc_h2o(b)
        elif param == "h2d":
            b = input("输入十六进制数[0x开头]: ")
            b = b.strip()
            result = localapi.api_calc_h2d(b)
        elif param == "badd":
            a = input("输入二进制数A: ")
            b = input("输入二进制数B: ")
            result = localapi.api_calc_badd(a,b)
        elif param == "bsub":
            a = input("输入二进制数A: ")
            b = input("输入二进制数B: ")
            result = localapi.api_calc_bsub(a,b)
        elif param == "bmul":
            a = input("输入二进制数A: ")
            b = input("输入二进制数B: ")
            result = localapi.api_calc_bmul(a,b)
        elif param == "bdiv":
            a = input("输入二进制数A: ")
            b = input("输入二进制数B: ")
            result = localapi.api_calc_bdiv(a,b)
        elif param == "hadd":
            a = input("输入十六进制数A: ")
            b = input("输入十六进制数B: ")
            result = localapi.api_calc_hadd(a,b)
        elif param == "hsub":
            a = input("输入十六进制数A: ")
            b = input("输入十六进制数B: ")
            result = localapi.api_calc_hsub(a,b)
        elif param == "hmul":
            a = input("输入十六进制数A: ")
            b = input("输入十六进制数B: ")
            result = localapi.api_calc_hmul(a,b)
        elif param == "hdiv":
            a = input("输入十六进制数A: ")
            b = input("输入十六进制数B: ")
            result = localapi.api_calc_hdiv(a,b)
        else:
            printer.warn("参数错误!")
        printer.test("结果: " + str(result))
    def do_exit(self,param):
        printer.warn("退出!")
        sys.exit()
if __name__ == "__main__":
    try:
        cli = Cli()
        cli.cmdloop()
    except Exception as e:
        traceback.print_exc()
        print("wrong!")