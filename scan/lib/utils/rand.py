#!/usr/bin/env python
# -*- coding:utf-8 -*-

from time import strftime
from random import randint,choice
import string

def r_time():
    """
    Return
        返回随机数字,按当前格式化时间做种子
    """
    t = randint(0,int(strftime('%y%m%d')))
    return t
def r_string(n):
    """
    Args:
        字符串长度
    Return
        返回随机字符串,大小写字母组合
    """
    s = "".join([choice(string.ascii_letters) for _ in range(0,int(n))])
    return s

