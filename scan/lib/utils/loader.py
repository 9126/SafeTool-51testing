#!/usr/bin/env python
# -*- coding:utf-8 -*-

import hashlib
import importlib
from importlib.abc import Loader
import os
from lib.utils.settings import ROOT_NAME,P_ATTACKS_PATH
def get_md5(value):
    if isinstance(value,str):
        value = value.encode(encoding="UTF-8")
    return hashlib.md5(value).hexdigest()

def load_string_to_module(code_string):
    try:
        module_name = 'plugins_{0}'.format(get_md5(code_string))
        file_path = ROOT_NAME + '{0}'.format(module_name)
        plugin_loader = PluginLoader(module_name,file_path)
        plugin_loader.set_data(code_string)
        spec = importlib.util.spec_from_file_location(module_name,file_path,loader=plugin_loader)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        return mod
    except ImportError:
        error_msg = "加载模块错误!"
        print(error_msg)
        raise
class PluginLoader(Loader):
    def __init__(self,fullname,path):
        self.fullname = fullname
        self.path = path
        self.data = None

    def set_data(self,data):
        self.data = data
    
    def get_filename(self):
        return self.path
    
    def get_data(self,filename):
        if filename.startswith(ROOT_NAME) and self.data:
            data = self.data
        else:
            raise ValueError("error-01")
        return data

    def exec_module(self,module):
        filename = self.get_filename()
        exec_code = self.get_data(filename)
        obj = compile(exec_code,filename,"exec",dont_inherit=True,optimize=-1)
        exec(obj,module.__dict__)
    