#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re

def getip(content):
	"""
    Func
        IP 正则表达式
    Args
        html 
    Return
        匹配内容
    """
	IP_LIST = re.findall(r'[0-9]+(?:\.[0-9]+){3}',content,re.I)
	if IP_LIST != None or IP_LIST != []:
		return IP_LIST