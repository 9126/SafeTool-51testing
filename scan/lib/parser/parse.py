#!/usr/bin/env python
# -*- coding:utf-8 -*-

import re
import string
from lib.parser.getip import *
from lib.parser.getmail import *

class parse:
    def __init__(self,content):
        self.content = content
    
    def clean(self):
        self.content = re.sub('<em>','',self.content)
        self.content = re.sub('<b>','',self.content)
        self.content = re.sub('</b>','',self.content)
        self.content = re.sub('<strong>','',self.content)
        self.content = re.sub('</strong>','',self.content)
        self.content = re.sub('</em>','',self.content)
        self.content = re.sub('<wbr>','',self.content)
        self.content = re.sub('</wbr>','',self.content)
        self.content = re.sub('<li>','',self.content)
        self.content = re.sub('</li>','',self.content)
        for x in ('>', ':', '=', '<', '/', '\\', ';', '&', '%3A', '%3D', '%3C'):
            self.content = self.content.replace(x,' ')

    def getmail(self):
        self.clean()
        v = getmail(self.content)
        return v

    def getip(self):
        self.clean()
        v = getip(self.content)
        return v       