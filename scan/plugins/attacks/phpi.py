#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import *

class phpi(Request):
    """
    PHP代码注入
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'phpi':None
        }
    def check(self):
        info("检测PHP代码注入漏洞...")
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True
        for payload in php():
            if self.data:
                rPayload = preplace(self.url,payload,self.data)
                for data in rPayload.run():
                    if "\"" in payload:
                        payload = payload.split('"')[1]
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data=data)
                    if search(r"root\:\/bin\/bash|"+payload,req.content):
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            else:
                urls = preplace(self.url,payload,None)
                for url in urls.run():
                    if "\"" in payload:
                        payload = payload.split('"')[1]
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    if search(r"root\:\/bin\/bash|"+payload,req.content):
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似存在PHP代码注入漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['phpi'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("疑似存在PHP代码注入漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['phpi'] = PAYLOAD
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = phpi(kwargs,url,data)
    result = scan.check()
    return result
